var express = require('express');
var app = express();
var server = require('http').createServer(app);
var io = require('socket.io')(server);
var MongoClient = require('mongodb').MongoClient;
var url = "mongodb://localhost:27017/";
var argv = require('optimist').argv;
var port = argv.p;
var sockets = {};
var redisPubSub = require('node-redis-pubsub');
var redisPubSubConfig = {
    host: "localhost",
    port: 6379,
    scope: "deliverySystem"
};
var redisPubSubClient = new redisPubSub(redisPubSubConfig);
server.listen(port, function () {
    console.log('listening on *:' + port);
});

try {

    redisPubSubClient.on('publishEvent', function (data, channel) {
        console.log("Data", data);
		//console.log("Room", data.room);
		if (data.acknowledgement) {
			var roomSockets = sockets[data.room];
			//console.log('Room', data.room);
			//console.log('RoomSockets', roomSockets);
			roomSockets.forEach(function(socket) {
				let sendTimestamp = Math.floor(Date.now()/1000);
				socket.emit(data.event, data.data, (response) => {
					//console.log("Response", response); // ok
					MongoClient.connect(url, function(err, db) {
						if (err) throw err;
						var dbo = db.db("delivery_system_location");
						var log = {
							userId: socket.user_id, 
							event: data.event, 
							data: data.data, 
							sent_at: sendTimestamp,
							received_at: Math.floor(Date.now()/1000)
						};
						let resp = dbo.collection("customers").insertOne(log, function(err, res) {
							if (err) throw err;
							console.log("1 document inserted");
							db.close();
						});
						console.log('resp', resp);
					});
				});
			});
		} else {
			io.to(data.room).emit(data.event, data.data);
		}
    });

    io.on('connection', function (socket) {
		//console.log("Connected");
		let id = socket.handshake.query.id;
		socket.user_id = id;
		socket.on("subscribe", (data, callback) => {
			console.log("Subscribe Data", data);
			var room = data.room;
			if (!(room in sockets)) {
				sockets[room] = [];
			}
			sockets[room].push(socket);
			socket.join(room);
			//console.log("Room Sockets", sockets[room]);
			//console.log("Socket", socket);
			//console.log("Socket Rooms", socket.rooms);
        });
		
        socket.on("chat_message", (data, callback) => {
			console.log("callback", callback);
			console.log("Socket Data", data);
			var obj = {
				"event": "chat_message",
				"data": data.message,
				"acknowledgement": callback ? true : false,
				"room": data.room
			};
            redisPubSubClient.publish("publishEvent", obj);

        });
		
		
		
		socket.on("disconnect", () => {
		  //console.log("Disconnected"); // false
		});

    });

} catch (e) {
    console.log(e);
}
